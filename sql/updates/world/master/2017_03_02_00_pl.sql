DELETE FROM `creature` WHERE `guid` BETWEEN 7001757 AND 7002487;
DELETE FROM `creature_addon` WHERE `guid` BETWEEN 7001757 AND 7002487;

DELETE FROM `gameobject` WHERE `guid` BETWEEN 7000099 AND 7001144;
DELETE FROM `gameobject_addon` WHERE `guid` BETWEEN 7000099 AND 7001144;
